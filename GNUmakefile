# SPDX-License-Identifier: GPL-3.0-or-later

MKDIR ?= mkdir -p
RMDIR ?= $(RM) -r
EMACS ?= emacs
LATEXMK ?= latexmk
PDF2SVG ?= pdf2svg

TARGET = RoboFish Track Format

.PHONY: pdf
pdf: build/main.pdf

build/%.pdf: build/%.tex
	$(LATEXMK) -pdf -pdflatex="pdflatex -interaction=nonstopmode" -cd $<

build/%.tex: src/%.org
	$(MKDIR) img
	$(EMACS) --batch --load publish.el $(<:src/%=%) latex

.PRECIOUS: img/%.pdf
img/%.svg: img/%.pdf
	${PDF2SVG} $< $@

.PRECIOUS: build/main.tex
img/%.pdf: build/main.tex ;

.PHONY: html
html: build/main.html img/hdf_example.svg img/world_space_2d.svg img/world_space_3d.svg

build/%.html: src/%.org
	$(EMACS) --batch --load publish.el $(<:src/%=%) html

.PHONY: dist
dist: pdf html
	$(MKDIR) dist/img
	cp build/main.pdf "dist/${TARGET} - $(VERSION).pdf"
	cp build/main.html "dist/${TARGET} - $(VERSION).html"
	cp img/*.svg dist/img/
	sed -i 's|"\.\./|"|g' "dist/${TARGET} - $(VERSION).html"

VERSION := $(shell git describe --tags --dirty --always)
.PHONY: pkg
pkg: dist
	$(MKDIR) pkg
	echo $(VERSION)
	tar -C dist -cJf "pkg/${TARGET} - $(VERSION).tar.xz" .


.PHONY: clean
clean:
	$(RMDIR) build
	$(RM) img/*.pdf

.PHONY: distclean
distclean: clean
	$(RMDIR) dist

.PHONY: pkgclean
pkgclean: distclean
	$(RMDIR) pkg

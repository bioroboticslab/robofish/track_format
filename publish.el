;; SPDX-License-Identifier: GPL-3.0-or-later

(setq backup-inhibited t)

(org-babel-do-load-languages
 'org-babel-load-languages '((latex . t)))

(setq org-confirm-babel-evaluate nil)
(setq org-publish-use-timestamps-flag nil)

(setq org-html-postamble nil)
(setq org-html-preamble-format
		'(("en"
			"<h1 class=\"title\">%t</h1>
<p class=\"subtitle\">%s</p>
<br/>
<p class=\"subtitle\">%a (%e)</p>")))

(setq target (elt argv 1))

(when (equal target "html")
  (setq org-export-with-title nil)
  )

(setq org-publish-project-alist
		`(("project"
			:base-directory "src"
			:exclude ".*"
			:include [,(elt argv 0)]
			:publishing-directory "build"
			:publishing-function ,(cond
										  ((equal target "latex") 'org-latex-publish-to-latex)
										  ((equal target "html") 'org-html-publish-to-html)
										  )
			)))
(org-publish "project")

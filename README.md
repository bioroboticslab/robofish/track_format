## Purpose

The RoboFish Track Format serves as an interface specification so that (1) all components of the RoboFish system can exchange tracking data in a standardized format and (2) to facility exchange of organism tracking data between researchers.

## Specification

Released versions can be found [here](https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format/-/releases).

## Implementation(s)

 - The reference Python implementation is available as [robofish.io](https://git.imp.fu-berlin.de/bioroboticslab/robofish/io).

## LICENSE

This work is licensed under GPL 3.0 (or any later version).
Individual files contain the following tag instead of the full license text:

`SPDX-License-Identifier: GPL-3.0-or-later`

This enables machine processing of license information based on the SPDX License Identifiers available here: https://spdx.org/licenses/
